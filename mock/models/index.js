import ResPartner from './res-partner'
import Product from './product-product'
import ResUsers from './res-users'
import HrEmployee from './hr-employee'
import HrExpense from './hr-expense'
import Soil from './soil'
import Ems from './ems'
import EduSafety from './edu-safety'

const Models = {
  ...Product,
  ...ResPartner,
  ...ResUsers,
  ...HrEmployee,
  ...HrExpense,
  ...Soil,
  ...Ems,
  ...EduSafety
}

/*

Models 是 所有的 odoo model
key 为 model name
values 是 model config
{
  metadate, // 字段定义, 等同于 odoo 的fields_get函数的 返回值
  records,  // 数据, 等同于 odoo 的 search 函数 的返回值
  extend, // 扩展函数

}

*/

export default Models
