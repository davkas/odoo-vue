// id, company_id, login, email, password, name
const Users_records = [
  [2, 1, 'admin', 'admin@saas', '123456', '平台管理员'],
  [2001, 2001, 'caiwu', 'admin@com1', '123456', '财务公司管理员'],
  [3001, 3001, 'caiwu', 'admin@com2', '123456', '学校风险管理员']
]

const Users = {
  metadata: {
    name: { type: 'char' },
    login: { type: 'char' },
    password: { type: 'char' },
    email: { type: 'char' },
    company_id: { type: 'many2one', relation: 'res.company' },
    company_ids: {
      type: 'many2many',
      relation: 'res.company',
      rel_table: 'rel_res_company_res_users',
      id1: 'uid',
      id2: 'cid'
    },
    display_name: { type: 'char' }
  },

  records: Users_records.map(item => {
    const [id, company_id, login, email, password, name] = item
    return { id, company_id, login, email, password, name }
  }),

  extend: BaseModel => {
    //
    class Model extends BaseModel {
      change_password(oldpsw, newpsw) {
        // console.log('session: ', this.session)
        const uid = this.session.uid
        this.browse_one(uid)

        return true
      }

      authenticate(db, login, password) {
        const domain = { login, password }
        const uid = this.search_one({ domain })
        return uid
      }

      get_userinfo(uid) {
        const user = this.browse_one(uid)
        return {
          uid: user.id,
          name: user.name,
          session_id: `${user.login}-token`
        }
      }
    }

    return Model
  }
}

// id, ptn, parent, company_registry, secondary_color, name
const Company_records = [
  [1, 1, null, 'base', 'base', '平台'],
  [1001, 1001, null, 'soil', 'soil', '土壤公司'],
  [2001, 2001, null, 'ems', 'ems', '财务公司'],
  [3001, 3001, null, 'edu-safety', 'edu-safety', '学校风险控制']
]

const Company = {
  metadata: {
    name: { type: 'char' },
    secondary_color: { type: 'char' },
    display_name: { type: 'char' },
    company_registry: { type: 'char' },
    email: { type: 'char' },
    phone: { type: 'char' },
    website: { type: 'char' },
    vat: { type: 'char' },
    partner_id: { type: 'many2one', relation: 'res.partner' },
    parent_id: { type: 'many2one', relation: 'res.company' },
    child_ids: {
      type: 'one2many',
      relation: 'res.company',
      related_field: 'parent_id'
    },
    user_ids: {
      type: 'many2many',
      relation: 'res.users',
      rel_table: 'rel_res_company_res_users',
      id1: 'cid',
      id2: 'uid'
    }
  },

  records: Company_records.map(item => {
    // id, ptn, parent, company_registry, name
    const [
      id,
      partner_id,
      parent_id,
      company_registry,
      secondary_color,
      name
    ] = item
    return {
      id,
      partner_id,
      parent_id,
      company_registry,
      secondary_color,
      name
    }
  }),

  extend: BaseModel => {
    //
    class Model extends BaseModel {
      create(vals) {
        const partner_id = this.env('res.partner').create({
          name: vals.name,
          is_company: true,
          email: vals.email,
          phone: vals.phone,
          website: vals.website,
          vat: vals.vat
        })
        vals.partner_id = partner_id

        return super.create(vals)
      }
      write(id, vals) {
        const ret = super.write(id, vals)
        const comp = this.browse_one(id)
        // console.log('xxxxxxxx, comp write', comp)
        this.env('res.partner').write(comp.partner_id, {
          name: vals.name,
          is_company: true,
          email: vals.email,
          phone: vals.phone,
          website: vals.website,
          vat: vals.vat
        })

        return ret
      }
    }

    return Model
  }
}

const rel_res_company_res_users = {
  records: [
    { cid: 1, uid: 2 },
    { cid: 2001, uid: 2001 }
  ]
}

export default {
  'res.users': Users,
  'res.company': Company,
  rel_res_company_res_users: rel_res_company_res_users
}
