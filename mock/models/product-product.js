// id, can_be_expensed,can_be_delivered , comp,  list_price, standard_price ,type, code, name
const product_product_records = [
  [1, true, false, 2001, 0, 1, 'service', null, '差旅费'],
  [2, true, false, 2001, 0, 1, 'service', null, '住宿费'],
  [3, false, true, 2001, 12, 7, 'service', 'ems', '快递'],
  [1101, false, true, 1001, 12, 7, 'service', '', '大气温度测试仪'],
  [1102, false, true, 1001, 12, 7, 'service', '', '土壤湿度测试仪'],
  [1103, false, true, 1001, 12, 7, 'service', '', '数据传输控制器'],
  [1104, false, true, 1001, 12, 7, 'service', '', '手持设备']
]

const product_product = {
  metadata: {
    company_id: { type: 'many2one', relation: 'res.company' },
    name: { type: 'char' },
    default_code: { type: 'char' },
    display_name: { type: 'char' },
    can_be_expensed: { type: 'boolean' },
    can_be_delivered: { type: 'boolean' },
    list_price: { type: 'float' },
    standard_price: { type: 'float' },
    type: {
      type: 'selection',
      selection: [
        ['consu', '易耗品'],
        ['service', '服务']
      ]
    },
    pricelist_item_ids: {
      type: 'one2many',
      relation: 'product.pricelist.item',
      related_field: 'product_id'
    }
  },
  records: product_product_records.map(item => {
    const [
      id,
      can_be_expensed,
      can_be_delivered,
      company_id,
      list_price,
      standard_price,
      type,
      default_code,
      name
    ] = item
    return {
      id,
      can_be_expensed,
      can_be_delivered,
      company_id,
      list_price,
      standard_price,
      type,
      default_code,
      name,
      display_name: name
    }
  })
}

// id, company_id, name
const product_pricelist_records = [
  [1101, 1001, '詹姆斯设备清单'],
  [1102, 1001, '王土地设备清单']
]

const product_pricelist = {
  metadata: {
    company_id: { type: 'many2one', relation: 'res.company' },
    name: { type: 'char' },
    partner_ids: {
      type: 'one2many',
      relation: 'res.partner',
      related_field: 'property_product_pricelist'
    },
    item_ids: {
      type: 'one2many',
      relation: 'product.pricelist.item',
      related_field: 'pricelist_id'
    }
  },
  records: product_pricelist_records.map(item => {
    const [id, company_id, name] = item
    return { id, company_id, name, display_name: name }
  })
}

// id, pricelist_id, product_id
const product_pricelist_item_records = [
  [1101, 1101, 1101],
  [1102, 1101, 1102],
  [1103, 1101, 1103],
  [1104, 1101, 1104],
  [1201, 1102, 1101],
  [1202, 1102, 1102],
  [1203, 1102, 1103],
  [1204, 1102, 1104]
]

const product_pricelist_item = {
  metadata: {
    pricelist_id: { type: 'many2one', relation: 'product.pricelist' },
    pricelist_name: { type: 'char', related: 'pricelist_id.name' },
    company_id: {
      type: 'many2one',
      relation: 'res.company',
      related: 'pricelist_id.company_id'
    },
    product_id: { type: 'many2one', relation: 'product.product' },
    product_name: { type: 'char', related: 'product_id.name' },
    name: { type: 'char', related: 'product_id.name' }
  },
  records: product_pricelist_item_records.map(item => {
    const [id, pricelist_id, product_id, name] = item
    return { id, pricelist_id, product_id, name }
  }),

  extend: BaseModel => {
    //
    class Model extends BaseModel {
      _compute_name(rec) {
        return {
          name: `${rec.product_name}`,
          display_name: `${rec.product_name}`
        }
      }

      // search_read2(query) {
      //   console.log('xxxx,', this.model, query)
      //   const res = super.search_read2(query)
      //   console.log('xxxx 1,', this.model, res)
      //   const res2 = res.map(item => {
      //     item.partner_id = item.pricelist_id__object.partner_ids

      //     return item
      //   })
      //   console.log('xxxx 2,', this.model, res2)
      //   return res2
      // }
    }

    return Model
  }
}

export default {
  'product.product': product_product,
  'product.pricelist': product_pricelist,
  'product.pricelist.item': product_pricelist_item
}
