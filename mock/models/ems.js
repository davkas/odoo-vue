import api from '../odooserver/api'

// id, comp, ptn, prd, price, qty, type, state,  date, name
const ems_order_records = [
  [1, 2001, 2201, 3, 12, 11, 'in_delivery', 'draft', '2019-12-12', '发票'],
  [2, 2001, 2201, 3, 12, 2, 'out_delivery', 'draft', '2019-12-12', '账单']
]

const ems_order = {
  metadata: {
    name: { type: 'char' },
    date: { type: 'date' },
    company_id: { type: 'many2one', relation: 'res.company' },
    partner_id: { type: 'many2one', relation: 'res.partner' },
    product_id: { type: 'many2one', relation: 'product.product' },
    unit_amount: { type: 'float', related: 'product_id.list_price' },
    quantity: { type: 'float' },
    total_amount: { type: 'float', compute: '_compute_total' },
    type: {
      type: 'selection',
      selection: [
        ['in_delivery', '寄到代账公司'],
        ['out_delivery', '寄到客户']
      ]
    },

    state: {
      type: 'selection',
      selection: [
        ['draft', '草稿'],
        ['confirm', '确认'],
        ['deliver', '已发送'],
        ['recieve', '已接收'],
        ['post', '已记账'],
        ['done', '已支付'],
        ['cancel', '已取消']
      ]
    }
  },

  records: ems_order_records.map(item => {
    // id, comp, ptn, prd, price, qty, type, state,  date, name
    const [
      id,
      company_id,
      partner_id,
      product_id,
      unit_amount,
      quantity,
      type,
      state,
      date,
      name
    ] = item
    return {
      id,
      company_id,
      partner_id,
      product_id,
      unit_amount,
      quantity,
      type,
      state,
      date,
      name
    }
  }),

  extend: BaseModel => {
    //
    class Model extends BaseModel {
      @api.depends('unit_amount', 'quantity')
      _compute_total(rec) {
        return {
          total_amount: rec.unit_amount * rec.quantity
        }
      }

      action_draft(rid) {
        return this.write(rid, { state: 'draft' })
      }
      action_cancel(rid) {
        return this.write(rid, { state: 'cancel' })
      }
      action_confirm(rid) {
        return this.write(rid, { state: 'confirm' })
      }
      action_deliver(rid) {
        return this.write(rid, { state: 'deliver' })
      }
      action_recieve(rid) {
        return this.write(rid, { state: 'recieve' })
      }
      action_post(rid) {
        return this.write(rid, { state: 'post' })
      }
      action_done(rid) {
        return this.write(rid, { state: 'done' })
      }
    }

    return Model
  }
}

export default {
  'ems.order': ems_order
}
