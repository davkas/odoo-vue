import { parseTime } from '../../src/utils'

const TestItem = {
  metadata: {
    name: { type: 'char' },
    display_name: { type: 'char' }
  },
  records: [
    { id: 1, name: '温度', display_name: '大气温度' },
    { id: 2, name: '湿度', display_name: '土壤湿度' }
  ]
}

// id, partner, date,
const Test_records_list = [
  [1, 1101, '2019-1-1'],
  [2, 1101, '2019-1-2'],
  [3, 1101, '2019-1-3'],
  [4, 1101, '2019-1-4'],
  [5, 1101, '2019-1-5'],
  [6, 1101, '2019-1-6'],
  [7, 1102, '2019-1-1'],
  [8, 1102, '2019-1-2'],
  [9, 1102, '2019-1-3'],
  [10, 1102, '2019-1-4'],
  [11, 1102, '2019-1-5'],
  [12, 1102, '2019-1-6']
]

const Test = {
  metadata: {
    partner_id: { type: 'many2one', relation: 'res.partner' },
    country_id: {
      type: 'many2one',
      relation: 'res.country',
      related: 'partner_id.country_id'
    },
    state_id: {
      type: 'many2one',
      relation: 'res.country.state',
      related: 'partner_id.state_id'
    },
    partner_name: { type: 'char', related: 'partner_id.name' },
    country_name: { type: 'char', related: 'country_id.name' },
    state_name: { type: 'char', related: 'state_id.name' },
    date: { type: 'date' },
    name: { type: 'char', compute: '_compute_name' },
    display_name: { type: 'char', compute: '_compute_name' },
    line_ids: {
      type: 'one2many',
      relation: 'soil.test.line',
      related_field: 'test_id'
    }
  },

  records: Test_records_list.map(item => {
    const [id, partner_id, date] = item
    return { id, partner_id, date }
  }),

  extend: BaseModel => {
    //
    class Model extends BaseModel {
      _compute_name(rec) {
        const name = `${parseTime(rec.date, '{y}-{m}-{d}')},${
          rec.partner_name
        },${rec.state_name},${rec.country_name}`

        return { name, display_name: name }
      }
    }

    return Model
  }
}

// id, test_id, item_id, value
const TestLine_records_list = [
  [1, 1, 1, 18],
  [2, 2, 1, 28],
  [3, 3, 1, 21],
  [4, 4, 1, 23],
  [5, 5, 1, 20],
  [6, 6, 1, 27],

  [7, 1, 2, 66],
  [8, 2, 2, 44],
  [9, 3, 2, 55],
  [10, 4, 2, 43],
  [11, 5, 2, 55],
  [12, 6, 2, 55],

  [13, 7, 1, 10],
  [14, 8, 1, 13],
  [15, 9, 1, 9],
  [16, 10, 1, 15],
  [17, 11, 1, 11],
  [18, 12, 1, 13],

  [19, 7, 2, 66],
  [20, 8, 2, 52],
  [21, 9, 2, 49],
  [22, 10, 2, 52],
  [23, 11, 2, 44],
  [24, 12, 2, 55]
]

const TestLine = {
  metadata: {
    test_id: { type: 'many2one', relation: 'soil.test' },
    item_id: { type: 'many2one', relation: 'soil.test.item' },
    value: { type: 'float' },
    date: { type: 'date', related: 'test_id.date' },
    partner_id: {
      type: 'many2one',
      relation: 'res.partner',
      related: 'test_id.partner_id'
    },
    country_id: {
      type: 'many2one',
      relation: 'res.country',
      related: 'partner_id.country_id'
    },
    state_id: {
      type: 'many2one',
      relation: 'res.country.state',
      related: 'partner_id.state_id'
    }
  },

  records: TestLine_records_list.map(item => {
    const [id, test_id, item_id, value] = item
    return { id, test_id, item_id, value }
  }),

  extend: BaseModel => {
    //
    class Model extends BaseModel {}

    return Model
  }
}

export default {
  'soil.test.item': TestItem,
  'soil.test': Test,
  'soil.test.line': TestLine
}
