import { parseTime } from '../../src/utils'

const find_or_create_for_emp_month = (self, values) => {
  // parseTime(rec[fld], '{y}-{m}-{d}')
  const { employee_id, date_month: date_month2 } = values
  const date_month = `${parseTime(date_month2, '{y}-{m}')}-01`
  const res = self.search_read2({ domain: { employee_id, date_month }})
  if (res.length) {
    return res[0].id
  } else {
    return self.super().create2({ ...values, date_month })
  }
}

const write_for_emp_month = (self, rid, values) => {
  // parseTime(rec[fld], '{y}-{m}-{d}')
  const vals = { ...values }
  delete vals.employee_id
  delete vals.date_month
  return self.super().write2(rid, vals)
}

export default { find_or_create_for_emp_month, write_for_emp_month }
