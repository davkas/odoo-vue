const PartnerTitle = {
  metadata: {
    name: { type: 'char' },
    display_name: { type: 'char' }
  },
  records: [
    { id: 1, name: '先生2', display_name: '先生' },
    { id: 2, name: '女士2', display_name: '女士' }
  ]
}

const PartnerCategory = {
  metadata: {
    name: { type: 'char' },
    display_name: { type: 'char' }
  },
  records: [
    { id: 1, name: 'A', display_name: 'A' },
    { id: 2, name: 'B', display_name: 'B' },
    { id: 3, name: 'C', display_name: 'C' }
  ]
}

const Country = {
  metadata: {
    name: { type: 'char' },
    display_name: { type: 'char' }
  },
  records: [
    { id: 1, name: '中国', display_name: '中国' },
    { id: 2, name: '美国', display_name: '美国' },
    { id: 3, name: '加拿大', display_name: '加拿大' }
  ]
}

const CountryState_records = [
  [1, 1, '北京市'],
  [2, 1, '河北省'],
  [3, 1, '天津市'],
  [4, 2, '加州'],
  [5, 2, '德州'],
  [6, 2, '纽约州'],
  [7, 3, '温哥华'],
  [8, 3, '多伦多'],
  [9, 3, '魁北克']
]

const CountryState = {
  metadata: {
    name: { type: 'char' },
    display_name: { type: 'char' },
    country_id: { type: 'many2one', relation: 'res.country' }
  },
  records: CountryState_records.map(item => {
    const [id, country_id, name] = item
    return { id, country_id, name, display_name: name }
  })
}
// id, type, iscomp, comp,  company_name,  parent, name, email,
// country, state,   date, title,   catag

// id, type, iscomp, comp,  comment,  parent, name, email,
const Partner_records_list1 = [
  [1, 'contact', true, 1, 'ems', null, '平台', 'n1@123'],
  [3, 'contact', false, 1, 'ems', 2, '张三', 'n1@123'],
  [1001, 'contact', true, 1001, 'soil', null, '土壤公司', 'n1@123'],
  [1101, 'contact', true, 1001, 'soil,customer', null, '詹姆斯', 'n1@123'],
  [1102, 'contact', true, 1001, 'soil,customer', null, '王土地', 'n1@123'],
  [2001, 'contact', true, 2001, 'ems', null, '财务公司', 'n1@123'],
  [2201, 'contact', true, 2001, 'ems,customer', null, '某贸易公司', '']
]

// country, state,   date, title, pricelist
const Partner_records_list2 = {
  1: [1, 1, '2019-1-2', 1],
  3: [1, 1, '2019-1-2', 1],
  1001: [1, 1, '2019-1-2', 1],
  1101: [2, 4, '2019-1-2', 1, 1101],
  1102: [1, 1, '2019-1-2', 1, 1102],
  2001: [1, 1, '2019-1-2', 1],
  2201: [1, 1, '2019-1-2', 1]
}

const Partner_records_list = Partner_records_list1.map(item => {
  return [...item, ...Partner_records_list2[item[0]]]
})

const Partner = {
  metadata: {
    comment: { type: 'text' },
    company_id: { type: 'many2one', relation: 'res.company' },

    // mock 中借用这个字段 区分不同的 业务
    company_registry: { type: 'char', related: 'company_id.company_registry' },
    name: { type: 'char' },
    display_name: { type: 'char' },
    function: { type: 'char' },
    email: { type: 'char' },
    date: { type: 'date' },
    is_company: { type: 'boolean' },
    type: {
      type: 'selection',
      selection: [
        ['contact', '联系人'],
        ['address', '地址']
      ]
    },
    country_id: { type: 'many2one', relation: 'res.country' },
    state_id: { type: 'many2one', relation: 'res.country.state' },
    title: { type: 'many2one', relation: 'res.partner.title' },
    title_name: { type: 'char', related: 'title.name' },
    category_id: {
      type: 'many2many',
      relation: 'res.partner.category',
      rel_table: 'rel_res_partner_res_partner_category',
      id1: 'pid',
      id2: 'cid'
    },
    bank_ids: {
      type: 'one2many',
      relation: 'res.partner.bank',
      related_field: 'partner_id'
    },
    parent_id: { type: 'many2one', relation: 'res.partner' },
    user_id: { type: 'many2one', relation: 'res.users' },
    child_ids: {
      type: 'one2many',
      relation: 'res.partner',
      related_field: 'parent_id'
    },

    property_product_pricelist: {
      type: 'many2one',
      relation: 'product.pricelist'
    },
    product_pricelist_item_ids: {
      type: 'one2many',
      relation: 'product.pricelist.item',
      related_field: 'pricelist_id'
    }
  },

  // id, type, iscomp, comp, comment,  parent, name, email,
  // country, state, date, title,   catag

  records: Partner_records_list.map(item => {
    const [
      id,
      type,
      is_company,
      company_id,
      comment,
      parent_id,
      name,
      email,
      country_id,
      state_id,
      date,
      title,
      property_product_pricelist
    ] = item
    return {
      id,
      type,
      is_company,
      company_id,
      comment,
      parent_id,
      name,
      email,
      country_id,
      state_id,
      date,
      title,
      property_product_pricelist
    }
  }),

  extend: BaseModel => {
    //
    class Model extends BaseModel {
      main_partner() {
        return [2]
      }

      find_or_create(email) {
        return 3
      }
    }

    return Model
  }
}

const PartnerBank = {
  metadata: {
    acc_number: { type: 'char' },
    display_name: { type: 'char', expression: '$acc_number' },
    date: { type: 'date' },
    bank_id: { type: 'many2one', relation: 'res.bank' },
    bank_name: { type: 'char', related: 'bank_id.name' },
    partner_id: { type: 'many2one', relation: 'res.partner' }
  },

  records: [
    { id: 1, acc_number: '123', bank_id: 1, partner_id: 1 },
    { id: 2, acc_number: '234', bank_id: 1, partner_id: 1 }
  ]
}

const Bank = {
  metadata: {},
  records: [
    { id: 1, name: '民生', display_name: '民生' },
    { id: 2, name: '招商', display_name: '招商' }
  ]
}

const rel_res_partner_res_partner_category = {
  records: [
    { pid: 3, cid: 1 },
    { pid: 3, cid: 2 }
  ]
}

export default {
  'res.country': Country,
  'res.country.state': CountryState,
  'res.partner': Partner,
  'res.partner.title': PartnerTitle,
  'res.partner.category': PartnerCategory,
  'res.partner.bank': PartnerBank,
  'res.bank': Bank,
  rel_res_partner_res_partner_category
}
