// id, company_id, name
const hr_department_records = [
  [1, 2001, '行政部'],
  [2, 2001, '销售部']
]

const hr_department = {
  metadata: {
    name: { type: 'char' },
    display_name: { type: 'char' },
    company_id: { type: 'many2one', relation: 'res.company' },
    parent_id: { type: 'many2one', relation: 'hr.department' },
    manager_id: { type: 'many2one', relation: 'hr.employee' },
    jobs_ids: {
      type: 'one2many',
      relation: 'hr.job',
      related_field: 'department_id'
    },
    child_ids: {
      type: 'one2many',
      relation: 'hr.department',
      related_field: 'parent_id'
    },
    member_ids: {
      type: 'one2many',
      relation: 'hr.employee',
      related_field: 'department_id'
    }
  },
  records: hr_department_records.map(item => {
    const [id, company_id, name] = item
    return { id, company_id, name }
  })
}

const hr_job = {
  metadata: {
    name: { type: 'char' },
    display_name: { type: 'char' }
  },
  records: [
    { id: 1, name: '主管' },
    { id: 2, name: '职员' }
  ]
}

// id, comp, dept, job, group, work num, name,
const hr_employee_records = [
  [1, 2001, 1, 1, '1001', '赵武'],
  [2, 2001, 2, 2, '1002', '田文']
]

const hr_employee = {
  metadata: {
    name: { type: 'char' },
    barcode: { type: 'char' },
    company_id: { type: 'many2one', relation: 'res.company' },
    department_id: { type: 'many2one', relation: 'hr.department' },
    user_id: { type: 'many2one', relation: 'res.users' },
    job_id: { type: 'many2one', relation: 'hr.job' },
    display_name: { type: 'char', display: '[${barcode}]${name}' }
  },

  records: hr_employee_records.map(item => {
    const [id, company_id, department_id, job_id, barcode, name] = item
    return { id, company_id, department_id, job_id, barcode, name }
  })
}

export default {
  'hr.employee': hr_employee,
  'hr.department': hr_department,
  'hr.job': hr_job
}
