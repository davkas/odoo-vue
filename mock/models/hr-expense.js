// id, sheet, prd,date,  price
const hr_expense_records = [
  [1, 1, 1, '2019-12-12', 123.32, '出差北京'],
  [2, 1, 2, '2019-12-02', 223.32, '出差北京'],
  [3, 2, 1, '2019-12-12', 621.32, '出差上海'],
  [4, 2, 2, '2019-12-02', 822.32, '出差上海']
]

const hr_expense = {
  metadata: {
    name: { type: 'char' },
    sheet_id: { type: 'many2one', relation: 'hr.expense.sheet' },
    employee_id: {
      type: 'many2one',
      relation: 'hr.employee',
      related: 'sheet_id.employee_id'
    },
    company_id: {
      type: 'many2one',
      relation: 'res.company',
      related: 'employee_id.company_id'
    },
    date: { type: 'date' },
    product_id: { type: 'many2one', relation: 'product.product' },
    unit_amount: { type: 'float' },
    total_amount: { type: 'float', expression: '$unit_amount' }
  },
  records: hr_expense_records.map(item => {
    const [id, sheet_id, product_id, date, unit_amount, name] = item
    return { id, sheet_id, product_id, date, unit_amount, name }
  })
}

// id,emp, user, date,type
const hr_expense_sheet_records = [
  [1, 1, 2001, '2019-12-12', 'draft', '出差北京'],
  [2, 1, 2001, '2019-12-12', 'draft', '出差上海']
]

const hr_expense_sheet = {
  metadata: {
    name: { type: 'char' },
    state: {
      type: 'selection',
      selection: [
        ['draft', '草稿'],
        ['submit', '已提交'],
        ['approve', '已审批'],
        ['post', '已记账'],
        ['done', '已支付'],
        ['cancel', '已拒绝']
      ]
    },
    accounting_date: { type: 'date' },
    employee_id: { type: 'many2one', relation: 'hr.employee' },
    user_id: { type: 'many2one', relation: 'res.users' },
    department_id: {
      type: 'many2one',
      relation: 'hr.department',
      related: 'employee_id.department_id'
    },
    company_id: {
      type: 'many2one',
      relation: 'res.company',
      related: 'employee_id.company_id'
    },
    expense_line_ids: {
      type: 'one2many',
      relation: 'hr.expense',
      related_field: 'sheet_id'
    },
    total_amount: { type: 'float', compute: '_compute_amount' }
  },

  records: hr_expense_sheet_records.map(item => {
    const [id, employee_id, user_id, accounting_date, state, name] = item
    return { id, employee_id, user_id, accounting_date, state, name }
  }),

  extend: BaseModel => {
    //
    class Model extends BaseModel {
      _onchange_employee_id(rid) {
        return
      }
      action_submit_sheet(rid) {
        this.write(rid, { state: 'submit' })
        return true
      }
      reset_expense_sheets(rid) {
        this.write(rid, { state: 'draft' })
        return true
      }
      refuse_sheet(rid, reason) {
        // console.log('xxx, refuse_sheet, ', rid, reason)
        this.write(rid, { state: 'cancel' })
        return true
      }
      approve_expense_sheets(rid) {
        this.write(rid, { state: 'approve' })
        return true
      }
      action_sheet_move_create(rid) {
        this.write(rid, { state: 'post' })
        return true
      }

      paid_expense_sheets(rid) {
        this.write(rid, { state: 'done' })
        return true
      }

      _compute_amount(rec) {
        // console.log('ref 8888', this.model)

        const ref = this.env(this.metadata.expense_line_ids.relation).browse(
          rec.expense_line_ids,
          {
            fields: { total_amount: null, unit_amount: null }
          }
        )
        // console.log('ref', ref)
        return {
          total_amount: ref.reduce((acc, cur) => {
            acc = acc + cur.total_amount
            return acc
          }, 0)
        }
      }
    }

    return Model
  }
}

export default {
  'hr.expense.sheet': hr_expense_sheet,
  'hr.expense': hr_expense
}
