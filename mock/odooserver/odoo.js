import BaseModel from './base-models'
import Models from '../models'

import session from './session'

const env = model => {
  //
  const options = Models[model] || {}
  // console.log('xxxxx,env,', model, options)

  const { metadata, records, extend } = options

  const MyClass = extend ? extend(BaseModel) : BaseModel
  return new MyClass(model, { metadata, records, env, session, Models })
}

/*
env 对外的函数
参数 model, {metadata, records}
返回值 一个对象
{
  model, // model name
  env, // 函数
  super, // 函数 返回 父类
  create, write, search_read, ... // 其他函数

}
*/
export default { env }
