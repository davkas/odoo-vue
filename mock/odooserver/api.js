function depends() {
  // TBD,  depends fields maybe 'o2m_ids.name'
  // so should be browsed with fields = {o2m_ids: {name}}
  const fields = [...arguments]
  return function(proto, key, descriptor) {
    const oldValue = descriptor.value
    descriptor.value = function() {
      const [rec] = arguments
      const new_rec = this.browse_one(rec.id, { fields, no_recursion: 1 })
      return oldValue.call(this, new_rec)
    }
  }
}

export default { depends }
