# 项目说明

这个项目是 基于 vue-element-admin, 调整接口, 访问 odoo  
项目中保留了很多 vue-element-admin 的痕迹. 许多已不再使用了, 但未做清理

### 已完成基础功能

登录/修改密码  
读取 odoo 多条数据, 列表展示  
读取 odoo 单条数据, 详情展示  
新增和编辑 Form  
新增/查看/编辑/删除按钮的隐藏配置  
Form 表单字段的只读/隐藏配置  
Form 表单中, many2one, selection 字段的 options 处理

详情页面工作流处理  
工作流处理中的 wizard 处理

odoo 报表的读取

odoo 图片的上传与读取(方法已找的, 待完善)

### 已完成的业务功能

res.company 公司  
res.users 用户  
多公司的创建, 用户的创建  
res.partner 业务伙伴  
客户基本信息管理  
公司客户及联系人  
联系人的类别 res.partner.category  
联系人的头衔 res.partner.title  
联系人的银行账号 res.partner.bank  
联系人的地址信息 res.country / res.country.state

产品基本信息 product.product  
产品类别 product.category

Hr  
员工基本信息 hr.employee  
部门 hr.departner  
职位 hr.job  
费用报销 hr.expense

销售模块  
销售订单 sale.order  
创建销售发票

财务模块  
销售发票 account.move  
销售收款 account.payment  
发票和收款单的核销  
财务报表

### 代码说明

#### vue.config.js 配置

项目根目录下的 vue.config.js 文件中, 通过 devServer.proxy 配置服务器  
vue-element-admin 有一个 mock 服务. mock 服务的代码在 项目根目录的 mock 目录下  
使用 odoo 服务, 这里指向 odoo 服务器地址  
使用 mock 服务, 这里指向 mock 服务地址

#### odoo.config.js 配置

项目根目录下的 odoo.config.js 文件中, 配置两个参数.  
一个是 odoo 服务器的数据库名  
一个 debug 开关

#### 路由及页面

src/router/index.js 中配置路由菜单  
src/Components 文件夹下是 vue-element-admin 定义的前端组件  
src/view 文件夹下是 vue-element-admin 的例子页面. 保留在此, 供学习用  
src/selfComponents 文件夹下是自定义的几个前端组件  
src/view1 文件夹下是所有的页面

#### odooapi

src/odooapi 文件夹下是访问 odoo 的 api 接口配置  
src/odooapi/index.js 是 入口文件.  
src/odooapi/modules 及 src/odooapi/odoo-models 文件夹下是与页面配套的配置文件  
src/odooapi/odoojs 文件夹下是 odoojs 源码.

#### odoojs

src/odooapi/odoojs 文件夹下是 odoojs 源码.  
src/odooapi/odoojs/index.js 是入口文件.

#### odoo 服务端

配合 odoojs , 在 odoo 服务端需要安装一个模块.  
https://gitee.com/odoowww/pyodoo/tree/master/addons/odoorpc

财务报表部分用到了 om_account_accountant  
http://www.odoo.com/apps/modules/13.0/om_account_accountant/

对 om_account_accountant 打了个补丁  
https://gitee.com/odoowww/pyodoo/tree/master/addons/rpc_report_accounting_pdf_reports

#### mock 服务

vue-element-admin 集成了 mock 服务. 实现前后端分离开发  
mock/index.js 是入口文件  
mock/odooserver 文件夹, 是模拟 odoo 服务的核心代码  
mock/models 文件夹, 是模拟 odoo 的 model

## 程序图片

![费用单列表页面](https://gitee.com/odoowww/vue-odoo/raw/master/docs/images/hr-expense-list.png)

![费用单详情页面](https://gitee.com/odoowww/vue-odoo/raw/master/docs/images/hr-expense-detail.png)

![进度条](https://gitee.com/odoowww/vue-odoo/raw/master/docs/images/edu-safety-progress.png)
