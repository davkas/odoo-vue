import odoo from '@/odooapi'

// import { parseTime } from '@/utils'

const List = {
  data() {
    return {
      listData: [],
      total: 0,

      ListQuery: {
        domain: {}
        // sort: [{ prop: 'id', order: 'descending' }]
        // page: 3
        // limit: 10,
      },

      optionForList: {
        indexColumn: { label: '#', width: 60 },
        pagination: {
          pageSizes: [2, 4, 6, 8, 10],
          total: 0,
          limit: 10
        },
        addBtn: true, // default : true,
        handleBtn: {
          // label: '操作',
          width: 200
          // viewBtn: true // default : false
          // editBtn: false, // default : true
          // delBtn: false, // default : true
        },

        formDialog: {
          // fullscreen: false,
          // labelWidth: 180
        }
      }
    }
  },
  computed: {
    //
    option: function() {
      //
      return {
        ...this.optionForList,
        handleBtn: { ...this.optionForList.handleBtn, viewPath: this.viewPath },
        pagination: { ...this.optionForList.pagination, total: this.total },

        columns: this.optionForList.columns.map(name => {
          return { name, ...this.fieldsForOption[name] }
        }),
        viewColumns: (this.optionForList.viewColumns || []).map(name => {
          return { name, ...this.fieldsForOption[name] }
        }),
        formColumns: this.optionForList.formColumns.map(name => {
          return { name, ...this.fieldsForOption[name] }
        })
      }
    }
  },

  async created() {
    this.Model = odoo.env(this.model)
  },

  methods: {
    async asyncSelectOptions(kwargs) {
      // call by ListPage
      return await this.Model.get_options(kwargs)
    },

    async flashData() {
      const { domain, sort, page = 1, limit } = this.ListQuery
      const fields = this.fieldsForSearch

      const order = (sort || [])
        .filter(item => item.order)
        .map(
          item => `${item.prop}${item.order === 'descending' ? ' desc' : ''}`
        )
        .join(', ')

      console.log('xx,sss1', this.ListQuery)
      console.log('xx,sss2', sort)
      console.log('xx,sss3', order)

      const res = await this.Model.search({
        domain,
        fields,
        order,
        page,
        limit: limit || (this.optionForList.pagination.pageSizes || [10])[0]
      })
      this.listData = res
    },

    async flashCount() {
      const { domain } = this.ListQuery
      const count = await this.Model.search_count(domain)
      this.total = count
    },

    handleRowLink(row) {
      console.log(row, this.viewPath)

      if (this.viewPath) {
        this.$router.push({
          path: this.viewPath,
          query: { id: row.id }
        })
      }
    },

    async handleRowSave(row, done, loading) {
      console.log('xxxxx, handleRowSave', row)
      const res = await this.Model.create(row)
      if (res) {
        done()
        await this.flashData()
        setTimeout(() => {
          loading()
        }, 1000)
      }
    },

    async handleRowUpdate(row, done, loading) {
      console.log('xxxxx, handleRowUpdate', row)
      const res = await this.Model.write(row)
      if (res) {
        done()
        await this.flashData()
        setTimeout(() => {
          loading()
        }, 1000)
      }
    },

    async handleRowDel(row, loading) {
      const rid = row.id
      const res = await this.Model.unlink(rid)
      if (res) {
        await this.flashData()
        await this.flashCount()
        setTimeout(() => {
          loading()
        }, 1000)
      }
    },

    handleSearchChange(domain) {
      this.ListQuery = { ...this.ListQuery, domain }
      this.flashCount()
      this.flashData()
    },

    handlePageChange({ page, limit }) {
      this.ListQuery = { ...this.ListQuery, page, limit }
      this.flashData()
    },

    handleSortChange(sort) {
      this.ListQuery = { ...this.ListQuery, sort }
      this.flashData()
    }
  }
}

export default List
