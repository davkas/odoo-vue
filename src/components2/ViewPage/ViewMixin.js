import odoo from '@/odooapi'

const List = {
  data() {
    return {
      viewData: {},
      optionForView: {
        //
      }
    }
  },
  computed: {
    //
    option: function() {
      //
      return {
        ...this.optionForView,

        leftColumns: this.optionForView.leftColumns.map(name => {
          return { name, ...this.fieldsForOption[name] }
        }),

        rightColumns: this.optionForView.rightColumns.map(name => {
          return { name, ...this.fieldsForOption[name] }
        }),
        tabs: Object.keys(this.optionForView.tabs).map(name => {
          const columns = (this.optionForView.tabs[name].columns || []).map(
            col => {
              return { name: col, ...this.fieldsForOption[col] }
            }
          )

          const columnName = this.optionForView.tabs[name].column
          const column = columnName
            ? { name: columnName, ...this.fieldsForOption[columnName] }
            : undefined

          return { name, ...this.optionForView.tabs[name], column, columns }
        })
      }
    }
  },

  async created() {
    this.Model = odoo.env(this.model)
  },

  methods: {
    async flashData() {
      const rid = this.$route.query.id || this.recId
      console.log('xxxx, rid:', rid)
      const fields = this.fieldsForBrowse
      this.viewData = await this.Model.browse_one(rid, {
        fields
      })
      console.log('xxxx, this.viewData:', this.viewData)
    }
  }
}

export default List
