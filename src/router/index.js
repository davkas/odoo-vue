import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar
    noCache: true                if set true, the page will no be cached(default is false)
    affix: true                  if set true, the tag will affix in the tags-view
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */

export const constantRoutes = [
  // Home
  {
    path: '/',
    component: Layout,
    hidden: true,
    redirect: '/home',
    children: [
      {
        path: 'home',
        component: () => import('@/views/home/index'),
        name: 'Home',
        meta: { title: '首页', icon: 'star', affix: true }
      }
    ]
  },

  // redirect
  {
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [
      {
        path: '/redirect/:path*',
        component: () => import('@/views/redirect/index')
      }
    ]
  },

  {
    path: '/app/login',
    component: () => import('@/viewsMobile/login'),
    hidden: true
  },

  // login
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },

  // auth-redirect
  {
    path: '/auth-redirect',
    component: () => import('@/views/login/auth-redirect'),
    hidden: true
  }

  // // 404
  // {
  //   path: '/404',
  //   component: () => import('@/views/error-page/404'),
  //   hidden: true
  // },

  // // 401
  // {
  //   path: '/401',
  //   component: () => import('@/views/error-page/401'),
  //   hidden: true
  // },

  // // documentation
  // {
  //   path: '/documentation',
  //   component: Layout,
  //   hidden: true,
  //   children: [
  //     {
  //       path: 'index',
  //       component: () => import('@/views/documentation/index'),
  //       name: 'Documentation',
  //       meta: { title: 'Documentation', icon: 'documentation' }
  //     }
  //   ]
  // },

  // // guide
  // {
  //   path: '/guide',
  //   component: Layout,
  //   hidden: true,
  //   redirect: '/guide/index',
  //   children: [
  //     {
  //       path: 'index',
  //       component: () => import('@/views/guide/index'),
  //       name: 'Guide',
  //       meta: { title: 'Guide', icon: 'guide', noCache: true }
  //     }
  //   ]
  // },

  // // profile
  // {
  //   path: '/profile',
  //   component: Layout,
  //   redirect: '/profile/index',
  //   hidden: true,
  //   children: [
  //     {
  //       path: 'index',
  //       component: () => import('@/views/profile/index'),
  //       name: 'Profile',
  //       meta: { title: 'Profile', icon: 'user', noCache: true }
  //     }
  //   ]
  // }
]

/**
 * asyncRoutes
 * the routes that need to be dynamically loaded based on user roles
 */
export const asyncRoutes = [
  {
    path: '/hdn',
    component: () => import('@/views/hdn/home'),
    hidden: true
  },
  {
    path: '/hdn1',
    component: () => import('@/views/hdn/home1'),
    hidden: true
  },

  // test
  {
    path: '/test',
    component: Layout,
    alwaysShow: true, // will always show the root menu
    name: 'test',
    // hidden: true,
    meta: {
      title: 'Test',
      icon: 'people',
      roles: ['admin', 'editor'] // you can set roles in root nav
    },
    children: [
      {
        path: 'test2',
        component: () => import('@/views/test/test'),
        name: 'test2',
        // hidden: true,
        meta: { title: 'Test', icon: 'dashboard' }
      },
      {
        path: 'test-list',
        component: () => import('@/views/test/test-list'),
        name: 'test-list',
        hidden: true,
        meta: { title: 'test-list', icon: 'dashboard' }
      },
      {
        path: 'test-detail',
        component: () => import('@/views/test/test-detail'),
        name: 'test-detail',
        hidden: true,
        meta: { title: 'test-detail', icon: 'dashboard' }
      }
    ]
  },

  // for mobile
  {
    path: '/app',
    component: () => import('@/viewsMobile/home'),
    hidden: true
  },
  {
    path: '/app/setting',
    component: () => import('@/viewsMobile/me/setting'),
    hidden: true
  },

  {
    path: '/app/changepsw',
    component: () => import('@/viewsMobile/me/changePsw'),
    hidden: true
  },

  {
    path: '/app/resetpsw',
    component: () => import('@/viewsMobile/me/resetPsw'),
    hidden: true
  },

  {
    path: '/app/mail/chat',
    component: () => import('@/viewsMobile/mail/chat'),
    hidden: true
  },
  {
    path: '/app/partner/card',
    component: () => import('@/viewsMobile/partner/partner-card'),
    hidden: true
  },

  {
    path: '/app/consult/survey-input',
    component: () => import('@/viewsMobile/consult/survey-input'),
    hidden: true
  },

  // for dict
  {
    path: '/dict',
    component: Layout,
    alwaysShow: true, // will always show the root menu
    name: 'Dict',
    hidden: true,
    meta: {
      title: 'Dict',
      icon: 'peoples',
      roles: ['admin', 'editor'] // you can set roles in root nav
    },
    children: [
      {
        path: 'raw-word',
        component: () => import('@/views/dict/dict-raw-word'),
        name: 'raw-word',
        // hidden: true,
        meta: { title: 'raw-word', icon: 'dashboard' }
      }
      // {
      //   path: 'word',
      //   component: () => import('@/views1/dictionary/dict-word'),
      //   name: 'word',
      //   // hidden: true,
      //   meta: { title: 'Word', icon: 'dashboard' }
      // },
      // {
      //   path: 'meaning',
      //   component: () => import('@/views1/dictionary/dict-meaning'),
      //   name: 'meaning',
      //   // hidden: true,
      //   meta: { title: 'Meaning', icon: 'dashboard' }
      // },
      // {
      //   path: 'example',
      //   component: () => import('@/views1/dictionary/dict-example'),
      //   name: 'example',
      //   // hidden: true,
      //   meta: { title: 'example', icon: 'dashboard' }
      // },
      // {
      //   path: 'learning',
      //   component: () => import('@/views1/dictionary/dict-learning'),
      //   name: 'learning',
      //   // hidden: true,
      //   meta: { title: 'learning', icon: 'dashboard' }
      // }
    ]
  },

  // me
  {
    path: '/user-me',
    component: Layout,
    alwaysShow: true, // will always show the root menu
    name: 'user-me',
    hidden: true,
    meta: {
      title: '个人信息',
      icon: 'people',
      roles: ['admin', 'editor'] // you can set roles in root nav
    },
    children: [
      // {
      //   path: 'res-users-me',
      //   component: () => import('@/views1/test/res/res-users-me'),
      //   name: 'res-users-me',
      //   // hidden: true,
      //   meta: { title: '登录信息', icon: 'dashboard' }
      // },
      // {
      //   path: 'changeUser',
      //   component: () => import('@/views1/user/userInfo'),
      //   name: 'promise',
      //   // hidden: true,
      //   meta: { title: '修改密码', icon: 'dashboard' }
      // }
    ]
  }
]

const createRouter = () =>
  new Router({
    // mode: 'history', // require service support
    scrollBehavior: () => ({ y: 0 }),
    routes: constantRoutes
  })

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
