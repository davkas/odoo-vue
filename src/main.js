import Vue from 'vue'

// import Cookies from 'js-cookie'
// 2019-11-16 By Master Zhang
// Cookies replaced by localStorage for google browser
//
import 'normalize.css/normalize.css' // a modern alternative to CSS resets

import Element from 'element-ui'
import './styles/element-variables.scss'

import '@/styles/index.scss' // global css

import MintUI from 'mint-ui'
import 'mint-ui/lib/style.css'

import App from './App'
import store from './store'
import router from './router'
import Print from 'vue-print-nb'

import './icons' // icon
import './permission' // permission control
import './utils/error-log' // error log

import * as filters from './filters' // global filters

import 'vx-easyui/dist/themes/default/easyui.css'
import 'vx-easyui/dist/themes/icon.css'
import 'vx-easyui/dist/themes/vue.css'
import EasyUI from 'vx-easyui'
Vue.use(EasyUI)

/**
 * If you don't want to use mock-server
 * you want to use MockJs for mock api
 * you can execute: mockXHR()
 *
 * Currently MockJs will be used in the production environment,
 * please remove it before going online! ! !
 */

// import { mockXHR } from '../mock'
// if (process.env.NODE_ENV === 'production') {
//   mockXHR()
// }

Vue.use(Element, {
  // size: Cookies.get('size') || 'medium' // set element-ui default size
  size: localStorage.getItem('size') || 'medium' // set element-ui default size
})

Vue.use(MintUI)

Vue.use(Print)

// register global utility filters
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
