import {
  // MessageBox,
  Message
} from 'element-ui'

import odooCreator from './odoojs'

const error = err => {
  console.log('odooapi, error,', err)
  console.log('odooapi, error.code,', err.code)
  console.log('odooapi, error.name,', err.name)
  console.log('odooapi, error.message,', err.message)
  console.log('odooapi, error.data,', err.data)

  Message({
    message: err.message,
    type: 'error',
    duration: 5 * 1000
  })
}
const odoo = odooCreator({ error })

export default odoo
