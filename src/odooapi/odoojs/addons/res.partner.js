const Model = {
  configs: {},

  extend: BaseClass => {
    class ModelClass extends BaseClass {
      global_domain(domain) {
        return super.global_domain(domain)
      }
      main_partner() {
        return this.call('main_partner')
      }

      find_or_create(email) {
        return this.call('find_or_create', [email])
      }

      consultation_get(partner_id) {
        return this.call('consultation_get', [partner_id])
      }

      async browse_one(rid, kwargs) {
        const res = await super.browse_one(rid, kwargs)
        const baseURL = process.env.VUE_APP_BASE_API
        const imgUrl = '/web/image'
        res.image = `${baseURL}${imgUrl}?model=${this.model}&id=${res.id}&field=image_1920`

        return res
      }

      async search(params = {}) {
        const res = await super.search(params)

        const baseURL = process.env.VUE_APP_BASE_API
        const imgUrl = '/web/image'
        // const imgField = 'image_128'

        res.map(row => {
          row.image = `${baseURL}${imgUrl}?model=${this.model}&id=${row.id}&field=image_1920`
          return row
        })

        return res
      }
    }
    return ModelClass
  }
}

export default Model
