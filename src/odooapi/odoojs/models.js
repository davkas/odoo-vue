// 简单的 domain, 都是 与 关系的, 可以用 dict 传参数,
// 复杂的 domain, 有 或 非 关系的, 依然可以用 list 格式
// global_domain 是个函数, 用于设置全局的/与用户有关的过滤条件
function _format_domain(self, domain_src = {}) {
  const global = self.global_domain()

  // eslint-disable-next-line object-curly-spacing
  const { domain_default = {} } = self.configs || {}
  let domain = []
  let domain_dict = {}
  if (Array.isArray(domain_default)) {
    domain = [...domain, ...domain_default]
  } else {
    domain_dict = { ...domain_dict, ...domain_default }
  }

  if (Array.isArray(domain_src)) {
    domain = [...domain, ...domain_src]
  } else {
    domain_dict = { ...domain_dict, ...domain_src }
  }

  if (Array.isArray(global)) {
    domain = [...domain, ...global]
  } else {
    domain_dict = { ...domain_dict, ...global }
  }

  return [domain, domain_dict]
}

export class ModelClass {
  constructor(options = {}) {
    const { model, rpc, env } = options
    this.model = model
    this.rpc = rpc
    this.env = env
  }

  get_userinfo() {
    return this.rpc.get_userinfo()
  }

  call(method, args, kwargs) {
    return this.rpc.call(this.model, method, args, kwargs)
  }

  // TBD  判断 数组或 字典
  global_domain(domain = {}) {
    // to be overrided
    // const domain = { ...domain_src }
    // const userinfo = this.get_userinfo()
    // const { uid } = userinfo
    // domain.create_uid = uid
    return domain
  }

  async get_options(fields) {
    const res = await this.call('get_options', [fields])
    return res
  }

  async default_get(fields, context) {
    const method = 'default_get'
    const args = [fields]
    const kwargs = { context }
    const res = await this.call(method, args, kwargs)
    return res
  }

  // 2019-12-8 no used
  async fields_get(kwargs) {
    // const { allfields, attributes } = kwargs
    const method = 'fields_get'
    const args = []
    const res = await this.call(method, args, kwargs)
    return res
  }

  // Not Used
  ref(xmlid) {
    // get model and id from xmlid
    return this.env('ir.model.data').call('xmlid_to_res_model_res_id', [
      xmlid,
      true
    ])
  }

  search_read(kwargs) {
    return this.call('search_read', [], kwargs)
  }

  async search_count(domain = {}) {
    const method = 'search_count2'
    const [domain_list, domain_dict] = _format_domain(this, domain)
    const args = []
    const kwargs = { domain: domain_list, domain2: domain_dict }
    return await this.call(method, args, kwargs)
  }

  async browse_one(id, query = {}) {
    const res = await this.browse(id, query)
    return res && res.length === 1 ? res[0] : {}
  }

  async browse(rid, query = {}) {
    // eslint-disable-next-line object-curly-spacing
    const { fields } = query
    const method = 'read2'

    // 判断 id 是数组或 int
    const args = [
      Array.isArray(rid) ? rid.map(item => parseInt(item)) : parseInt(rid)
    ]
    const kwargs = {
      fields: fields // || { display_name: null }
    }

    // read2 返回值是 数组
    const records = await this.call(method, args, kwargs)
    return records
  }

  async search(query = {}) {
    // const sss = await this.fields_get()
    // console.log('xxxxxxx,fields_get', sss)
    const {
      domain = {}, // 目前的前端 都是简单的 domian ={}
      fields = {},
      page = 1,
      limit = 0,
      order = ''
    } = query

    const [domain_list, domain_dict] = _format_domain(this, domain)

    const method = 'search_read2'
    const args = []
    const kwargs = {
      domain: domain_list,
      domain2: domain_dict,
      fields: fields || { name: null },
      offset: (page - 1) * limit,
      limit,
      order
    }

    const records = await this.call(method, args, kwargs)

    return records
  }

  async create(values = {}, kwargs = {}) {
    const method = 'create2'
    const args = [values]
    return await this.call(method, args, kwargs)
  }

  async write(values, kwargs = {}) {
    // console.log('xxxxx, write', values)
    const method = 'write2'
    const rid = values.id
    const values2 = { ...values }
    delete values2.id
    const args = [rid, values2]
    return await this.call(method, args, kwargs)
  }

  async unlink(rid) {
    const method = 'unlink'
    const args = [rid]

    return await this.call(method, args)
  }
}

const creater = options => {
  return new ModelClass(options)
}
export default creater
