import odoo from '@/odooapi'

const baseModel = {
  async search(payload) {
    // console.log('xxxxx, in baseModel search', payload)
    const { model, method = 'search' } = payload
    const payload2 = { ...payload }
    delete payload2.model
    delete payload2.method
    const Model = odoo.env(model)
    return await Model[method](payload2)
  },

  async browse_one(payload) {
    // console.log('xxxxx, in baseModel browse_one', payload)
    const { model, method = 'browse_one', id } = payload
    const payload2 = { ...payload }
    delete payload2.model
    delete payload2.method
    delete payload2.id
    const Model = odoo.env(model)
    return await Model[method](parseInt(id), payload2)
  },

  async write(payload) {
    // console.log('xxxxx, in baseModel browse_one', payload)
    const { model, method = 'write' } = payload
    const payload2 = { ...payload }
    delete payload2.model
    delete payload2.method
    const Model = odoo.env(model)
    return await Model[method](payload2)
  }
}

const state = {
  // dataList: [],
  // dataDict: {}
}

const mutations = {
  SET_DataList: (state, dataList) => {
    state.dataList = dataList
  },
  SET_DataDict: (state, row) => {
    state.dataDict = row
  },
  UPDATE_DataDict: (state, row) => {
    state.dataDict = { ...state.dataDict, ...row }
  }
}

const actions = {
  async _search({ commit, state, dispatch }, payload) {
    // console.log('xxxxx, in search', payload)
    const dataList = await baseModel.search(payload)
    commit('SET_DataList', dataList)
    return new Promise(resolve => {
      resolve(dataList)
    })
  },

  async _browse_one({ commit, state, dispatch }, payload) {
    const record = await baseModel.browse_one(payload)
    commit('SET_DataDict', record)
    return new Promise(resolve => {
      resolve(record)
    })
  },

  async _write({ commit, state, dispatch }, payload) {
    const record = await baseModel.write(payload)
    console.log('xxx,_wrtie ok ,', record, state)
    commit('SET_DataDict', record)
    return new Promise(resolve => {
      resolve(record)
    })
  },

  async write({ commit, state, dispatch }, payload) {
    return dispatch('_write', payload)
  },

  async search({ commit, state, dispatch }, payload) {
    return dispatch('_search', payload)
  },

  async browse_one({ commit, state, dispatch }, payload) {
    return dispatch('_browse_one', payload)
  }
}

const creator = (model, module) => {
  // console.log('module src: ', module)

  const {
    namespaced,
    state: state1,
    mutations: mutations1,
    actions: actions1
  } = module
  const actions2 = Object.keys(actions).reduce((acc, cur) => {
    acc[cur] = (vuex2, payload = {}) => {
      // console.log('xxxxx, in creator')
      return actions[cur](vuex2, { model, ...payload })
    }
    return acc
  }, {})

  return {
    namespaced,
    state: {
      dataList: [],
      dataDict: {},
      ...state,
      ...state1
    },
    mutations: { ...mutations, ...mutations1 },
    actions: { ...actions2, ...actions1 }
  }
}

export default creator

// export default {
//   namespaced: true,
//   state,
//   mutations,
//   actions
// }
