// import odoo from '@/odooapi'

import baseStore from '../baseStore'

const modelName = 'mail.channel'

const state = {}

const mutations = {}

const actions = {
  async search({ commit, state, dispatch }, payload) {
    return dispatch('_search', {
      method: 'search_private',
      order: 'id'
    })
  }
}

export default baseStore(modelName, {
  namespaced: true,
  state,
  mutations,
  actions
})
