// import odoo from '@/odooapi'

import baseStore from '../baseStore'

import odoo from '@/odooapi'

const modelName = 'res.partner'

const state = {
  //
}

const mutations = {
  //
}

const actions = {
  async search({ commit, state, dispatch }, payload) {
    return dispatch('_search', {
      ...payload,
      domain: { employee: true },
      fields: {
        name: null
      }
    })
  },
  async browse_one({ commit, state, dispatch }, { id }) {
    await dispatch('_browse_one', {
      id,
      fields: {
        name: null,
        title: null,
        comment: null
      }
    })
    const cnl = await odoo.env('mail.channel').find_channel_private(id)
    commit('UPDATE_DataDict', { channel: cnl })
    console.log('state.dataDict', state.dataDict)
    return state.dataDict
  },

  async channel_get({ commit, state, dispatch }, { id }) {
    const cnl = await odoo.env('mail.channel').channel_get([id])
    commit('UPDATE_DataDict', { channel: cnl })
    console.log('22222 state.dataDict', state.dataDict)
    return state.dataDict
  }
}

export default baseStore(modelName, {
  namespaced: true,
  state,
  mutations,
  actions
})
