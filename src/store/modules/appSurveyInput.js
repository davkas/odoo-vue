// import odoo from '@/odooapi'

import baseStore from '../baseStore'

import odoo from '@/odooapi'

const modelName = 'survey.user_input'

const state = {
  //
}

const mutations = {
  //
}

const actions = {
  async search({ commit, state, dispatch }, payload) {
    return dispatch('_search', payload)
  },
  async browse_one({ commit, state, dispatch }, payload) {
    return dispatch('_browse_one', { ...payload, method: 'browse2_one' })
  },
  async write({ commit, state, dispatch }, payload) {
    return dispatch('_write', { ...payload, method: 'write_line' })
  }
}

export default baseStore(modelName, {
  namespaced: true,
  state,
  mutations,
  actions
})
