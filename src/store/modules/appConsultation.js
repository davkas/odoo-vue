// import odoo from '@/odooapi'

import baseStore from '../baseStore'

import odoo from '@/odooapi'

const modelName = 'consult.consultation'

const state = {
  //
}

const mutations = {
  //
}

const actions = {
  async search({ commit, state, dispatch }, payload) {
    return dispatch('_search', payload)
  },
  async browse_one({ commit, state, dispatch }, payload) {
    return dispatch('_browse_one', payload)
  },

  async consultation_get({ commit, state, dispatch }, partner_id) {
    const Ptn = odoo.env('res.partner')
    const fields = {
      date: null,
      response_id: null
    }

    const consultation_id = await Ptn.consultation_get(partner_id)
    const consultation = await dispatch('browse_one', {
      id: consultation_id,
      fields
    })

    return consultation
  },

  async message_post({ commit, state, dispatch }, payload) {
    const { id } = payload
    const Model = odoo.env(modelName)

    // const payload2 = { ...payload }
    // delete payload2.id

    const channel = this.state.appChat.channel
    const user_partner_id = this.state.appMe.partner_id

    const payload2 = {
      body: '',
      author_id: user_partner_id,
      channel_ids: [channel.id]
    }

    const res2 = Model.call('message_post', [id], payload2)

    return new Promise(resolve => {
      resolve()
    })
  }
}

export default baseStore(modelName, {
  namespaced: true,
  state,
  mutations,
  actions
})
