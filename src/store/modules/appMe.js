import odoo from '@/odooapi'

const state = {
  isEmployee: false,
  partner: {},
  uid: undefined,
  partner_id: undefined
}

const mutations = {
  SET_ME: (state, { partner, uid, partner_id }) => {
    state.partner = { ...partner }
    state.isEmployee = partner.employee
    state.uid = uid
    state.partner_id = partner_id
  }
}

const actions = {
  async getInfo({ commit, state, dispatch }) {
    const { uid, partner_id } = await odoo.get_userinfo()
    const Ptn = odoo.env('res.partner')
    const partner = await Ptn.browse_one(partner_id, {
      fields: { name: null, employee: null, title: null }
    })
    console.log('xxxx, getInfo', partner)
    commit('SET_ME', { partner, uid, partner_id })
    return new Promise(resolve => {
      // commit('SET_CURRENT', current)
      resolve({ uid, partner_id, partner })
    })
  }
}
export default {
  namespaced: true,
  state,
  mutations,
  actions
}
