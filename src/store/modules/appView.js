const state = {
  home: 'home', // ['home', ... , 'me']
  login: 'login' // ['login', 'register', 'reset', 'mobile']
}

const mutations = {
  SET_Home: (state, current) => {
    state.home = current
  },
  SET_Login: (state, current) => {
    state.login = current
  }
}

const actions = {
  selectHome({ commit, state, dispatch }, selected) {
    return new Promise(resolve => {
      commit('SET_Home', selected)
      resolve()
    })
  },

  selectLogin({ commit, state, dispatch }, selected) {
    return new Promise(resolve => {
      commit('SET_Login', selected)
      resolve()
    })
  }
}
export default {
  namespaced: true,
  state,
  mutations,
  actions
}
