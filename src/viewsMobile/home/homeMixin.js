const Mixin = {
  data() {
    return {
      //
    }
  },
  computed: {
    isEmployee: function() {
      return this.$store.state.appMe.isEmployee
    }
  },

  async created() {
    this.$store.dispatch('appMe/getInfo')
  },

  methods: {
    async handleClickTabbar() {
      console.log(this.selected)
    }
  }
}

export default Mixin
